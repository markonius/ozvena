let ENTER = "Enter";
let NUM_ENTER = "NumpadEnter";

var bigHash = undefined;
var endHash = undefined;

var uninitializedSection;
var initializedSection;
var settingsSection;

var settingsErrorMessage;
var settingsElements;
var settingsSaveButton;

let placeholderText = "";

var accountsManaged = false;
var masterSettingsHeight = 0;
var secondarySettingsHeight = 0;

let defaultSettings = {
	"version": 7,
	"acceptedCookies": false,
	"master": {
		"N": 32768,
		"r": 8,
		"p": 8,
		"dkLen": 33, // byte length must be multiple of 3, base64 length must be multiple of 4
		"salt": "stolna, morska",
		"showHashcheck": true,
		"showConfirmField": false
	},
	"secondary": {
		"N": 2048,
		"r": 8,
		"p": 8,
		"dkLen": 18, // byte length must be multiple of 3, base64 length must be multiple of 4
		"salt": "stolna, morska",
		"clearSecondaryPassword": true,
		"hideAccount": false,
		"showConfirmField": true
	},
	"savedAccounts": [
	]
}
var settings = defaultSettings;


function start() {
	console.log("Window loaded");

	uninitializedSection = document.getElementById("uninitializedSection");
	initializedSection = document.getElementById("initializedSection");
	settingsSection = document.getElementById("settingsParent");

	settingsErrorMessage = document.getElementById("settingsErrorMessage");
	settingsElements = {
		"masterN": document.getElementById("masterN"),
		"masterr": document.getElementById("masterr"),
		"masterp": document.getElementById("masterp"),
		"masterdkLen": document.getElementById("masterdkLen"),
		"secondaryN": document.getElementById("secondaryN"),
		"secondaryr": document.getElementById("secondaryr"),
		"secondaryp": document.getElementById("secondaryp"),
		"secondarydkLen": document.getElementById("secondarydkLen"),
	}
	settingsSaveButton = document.getElementById("saveSettingsButton");

	initializedSection.style.display = "none";
	settingsSection.style.display = "none";
	document.getElementsByTagName("BODY")[0].style.display = "block";

	let masterPassword = document.getElementById("masterPassword");
	masterPassword.focus();
	masterPassword.addEventListener("keydown", function (event) {
		if (event.code === ENTER || event.code === NUM_ENTER) {
			document.getElementById("initializeButton").click();
		}
	});

	document.getElementById("confirmMasterPassword").addEventListener("keydown", function (event) {
		if (event.code === ENTER || event.code === NUM_ENTER) {
			document.getElementById("initializeButton").click();
		}
	});

	document.getElementById("account").addEventListener("keydown", function (event) {
		if (event.code === ENTER || event.code === NUM_ENTER) {
			document.getElementById("secondaryPassword").focus();
		}
	});

	document.getElementById("secondaryPassword").addEventListener("keydown", function (event) {
		if (event.code === ENTER || event.code === NUM_ENTER) {
			document.getElementById("generateButton").click();
		}
	});

	document.getElementById("confirmSecondaryPassword").addEventListener("keydown", function (event) {
		if (event.code === ENTER || event.code === NUM_ENTER) {
			document.getElementById("generateButton").click();
		}
	});

	document.getElementById("importInput").addEventListener("change", doImportSettings, false);

	// Load cookie
	let cookie = getCookie("settings");
	console.log("Cookie: " + cookie);
	if (cookie) {
		try {
			let s = JSON.parse(cookie);
			if (s.version < settings.version) {
				upgradeSettings(s);
			}
			else {
				settings = s;
			}
			if (settings.acceptedCookies) {
				saveCookie();
				hideCookieBanner();
			}
		}
		catch {
			console.log("Error loading cookie");
		}
	}

	populateSettingsSection();
}

function validateSettings() {
	var errorMessage = "";

	let masterN = getInt(settingsElements.masterN.value);
	if (isNaN(masterN) || !isPowerOf2(masterN)) {
		errorMessage += "N must be a power of 2.<br/>";
	}
	let masterr = getInt(settingsElements.masterr.value);
	if (isNaN(masterr)) {
		errorMessage += "r must be a number.<br/>";
	}
	let masterp = getInt(settingsElements.masterp.value);
	if (isNaN(masterp)) {
		errorMessage += "p must be a number.<br/>";
	}
	let masterdkLen = getInt(settingsElements.masterdkLen.value);
	if (isNaN(masterdkLen)) {
		errorMessage += "dkLen must be a number.<br/>";
	}

	let secondaryN = getInt(settingsElements.secondaryN.value);
	if (isNaN(secondaryN) || !isPowerOf2(secondaryN)) {
		errorMessage += "N must be a power of 2.<br/>";
	}
	let secondaryr = getInt(settingsElements.secondaryr.value);
	if (isNaN(secondaryr)) {
		errorMessage += "r must be a number.<br/>";
	}
	let secondaryp = getInt(settingsElements.secondaryp.value);
	if (isNaN(secondaryp)) {
		errorMessage += "p must be a number.<br/>";
	}
	let secondarydkLen = getInt(settingsElements.secondarydkLen.value);
	if (isNaN(secondarydkLen)) {
		errorMessage += "dkLen must be a number.<br/>";
	}

	if (errorMessage != "") {
		settingsErrorMessage.style.opacity = 1;
		settingsSaveButton.disabled = true;
	}
	else {
		settingsErrorMessage.style.opacity = 0;
		settingsSaveButton.disabled = false;
	}

	settingsErrorMessage.innerHTML = errorMessage;
	return errorMessage == "";
}

function getInt(value) {
	if (parseFloat(value) == parseInt(value))
		return Number(value);
	else
		return NaN;
}

function isPowerOf2(number) {
	return number && (number & (number - 1)) === 0;
}

function setAccountInputType() {
	let acc = document.getElementById("account");
	if (settings.secondary.hideAccount) {
		acc.type = "password";
		acc.nextElementSibling.style.display = "block";
		acc.nextElementSibling.nextElementSibling.style.display = "none";
	}
	else {
		acc.type = "text";
		acc.nextElementSibling.style.display = "none";
		acc.nextElementSibling.nextElementSibling.style.display = "block";
	}
}

function setConfirmFieldsVisibility() {
	let mc = document.getElementById("confirmMasterPasswordSection");
	if (settings.master.showConfirmField)
		mc.style.display = "unset";
	else
		mc.style.display = "none";

	let sc = document.getElementById("confirmSecondaryPasswordSection");
	if (settings.secondary.showConfirmField)
		sc.style.display = "unset";
	else
		sc.style.display = "none";
}

function upgradeSettings(oldSettings) {
	settings.acceptedCookies = oldSettings.acceptedCookies;

	settings.master.N = oldSettings.master.N;
	settings.master.r = oldSettings.master.r;
	settings.master.p = oldSettings.master.p;
	settings.master.dkLen = oldSettings.master.dkLen;
	settings.master.salt = oldSettings.master.salt;
	if (oldSettings.master.showHashcheck !== undefined)
		settings.master.showHashcheck = oldSettings.master.showHashcheck;
	if (oldSettings.master.showConfirmField !== undefined)
		settings.master.showConfirmField = oldSettings.master.showConfirmField;

	settings.secondary.N = oldSettings.secondary.N;
	settings.secondary.r = oldSettings.secondary.r;
	settings.secondary.p = oldSettings.secondary.p;
	settings.secondary.dkLen = oldSettings.secondary.dkLen;
	settings.secondary.salt = oldSettings.secondary.salt;
	settings.secondary.clearSecondaryPassword = oldSettings.secondary.clearSecondaryPassword;
	settings.secondary.hideAccount = oldSettings.secondary.hideAccount;
	if (oldSettings.secondary.showConfirmField !== undefined)
		settings.secondary.showConfirmField = oldSettings.secondary.showConfirmField;

	if (oldSettings.savedAccounts !== undefined)
		settings.savedAccounts = oldSettings.savedAccounts;

	console.log("Upgraded settings");
}

function populateSettingsSection() {
	document.getElementById("masterN").value = settings.master.N;
	document.getElementById("masterr").value = settings.master.r;
	document.getElementById("masterp").value = settings.master.p;
	document.getElementById("masterdkLen").value = settings.master.dkLen;
	document.getElementById("masterSalt").value = settings.master.salt;
	document.getElementById("showHashcheck").checked = settings.master.showHashcheck;
	document.getElementById("showMasterConfirm").checked = settings.master.showConfirmField;

	document.getElementById("secondaryN").value = settings.secondary.N;
	document.getElementById("secondaryr").value = settings.secondary.r;
	document.getElementById("secondaryp").value = settings.secondary.p;
	document.getElementById("secondarydkLen").value = settings.secondary.dkLen;
	document.getElementById("secondarySalt").value = settings.secondary.salt;
	document.getElementById("clearSPCheckbox").checked = settings.secondary.clearSecondaryPassword;
	document.getElementById("hideAccountCheckbox").checked = settings.secondary.hideAccount;
	document.getElementById("showSecondaryConfirm").checked = settings.secondary.showConfirmField;

	setAccountInputType();
	setConfirmFieldsVisibility();
}

// Cookies

function saveCookie() {
	console.log("Saved cookie");
	setCookie("settings", JSON.stringify(settings), 365);
}

function deleteCookie() {
	if (confirm("Are you sure you want to reset your settings?")) {
		settings = defaultSettings;
		saveCookie();
		eraseCookie("settings");
	}
}

function setCookie(name, value, days) {
	var expires = "";
	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		expires = "; expires=" + date.toUTCString();
	}
	document.cookie = name + "=" + (value || "") + expires + "; path=/";
}
function getCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
	}
	return null;
}
function eraseCookie(name) {
	document.cookie = name + '=; Max-Age=-99999999;';
}
// /Cookies

function toggleSettings() {
	let button = document.getElementById("settingsButton");
	let icons = button.getElementsByTagName("img");

	if (settingsSection.style.display == "none") {
		settingsSection.style.display = "flex";
		setTimeout(function () {
			settingsSection.style.opacity = "1";
			settingsSection.style.marginRight = "0";
			settingsSection.style.marginLeft = "0";
		}, 16);
		hideIcon(icons[0]);
		showIcon(icons[1]);

		let settingsMaster = document.getElementById("settings-master");
		let settingsSecondary = document.getElementById("settings-secondary");
		masterSettingsHeight = settingsMaster.clientHeight;
		secondarySettingsHeight = settingsSecondary.clientHeight;
		settingsMaster.style.height = masterSettingsHeight + "px";
		settingsSecondary.style.height = secondarySettingsHeight + "px";
	}
	else {
		settingsSection.style.opacity = "0";
		settingsSection.style.marginRight = "-100pt";
		settingsSection.style.marginLeft = "100pt";
		setTimeout(function () {
			settingsSection.style.display = "none";
			hideAccountManager();
		}, 200);
		showIcon(icons[0]);
		hideIcon(icons[1]);
	}
}

function showIcon(icon) {
	icon.style.opacity = "1";
	icon.style.transform = "scale(1, 1)";
}
function hideIcon(icon) {
	icon.style.opacity = "0";
	icon.style.transform = "scale(0, 0)";
}

function saveSettings() {
	settings.master.N = document.getElementById("masterN").value;
	settings.master.r = document.getElementById("masterr").value;
	settings.master.p = document.getElementById("masterp").value;
	settings.master.dkLen = document.getElementById("masterdkLen").value;
	settings.master.salt = document.getElementById("masterSalt").value;
	settings.master.showHashcheck = document.getElementById("showHashcheck").checked;
	settings.master.showConfirmField = document.getElementById("showMasterConfirm").checked;

	settings.secondary.N = document.getElementById("secondaryN").value;
	settings.secondary.r = document.getElementById("secondaryr").value;
	settings.secondary.p = document.getElementById("secondaryp").value;
	settings.secondary.dkLen = document.getElementById("secondarydkLen").value;
	settings.secondary.salt = document.getElementById("secondarySalt").value;
	settings.secondary.clearSecondaryPassword = document.getElementById("clearSPCheckbox").checked;
	settings.secondary.hideAccount = document.getElementById("hideAccountCheckbox").checked;
	settings.secondary.showConfirmField = document.getElementById("showSecondaryConfirm").checked;

	setAccountInputType();
	setConfirmFieldsVisibility();

	if (settings.acceptedCookies)
		saveCookie();
}

function importSettings() {
	let importInput = document.getElementById("importInput");
	importInput.click();
}
function doImportSettings() {
	let importInput = document.getElementById("importInput");
	if (importInput.files.length > 0) {
		let file = importInput.files[0];
		file.text()
			.then(function (text) {
				let oldSettings = settings;
				let s = JSON.parse(text);
				if (s.version < settings.version) {
					upgradeSettings(s);
				}
				else {
					settings = s;
				}
				populateSettingsSection();

				if (validateSettings()) {
					if (settings.acceptedCookies) {
						saveCookie();
					}
				}
				else {
					settings = oldSettings;
				}
			});
	}
}

function exportSettings() {
	let element = document.createElement("a");
	let settingsString = JSON.stringify(settings);
	element.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(settingsString));
	element.setAttribute("download", "ozvena-settings.crv");

	element.style.display = "none";
	document.body.appendChild(element);
	element.click();
	document.body.removeChild(element);
}

function getBuffer(string) {
	return new buffer.SlowBuffer(string.normalize("NFKC"), "utf8");
}

function getB64(bytes) {
	var binary = '';
	let len = bytes.length;
	for (var i = 0; i < len; i++) {
		binary += String.fromCharCode(bytes[i]);
	}
	var base64 = window.btoa(binary);
	while (base64[base64.length - 1] == '=') {
		base64 = base64.substring(0, base64.length - 1);
	}
	return base64;
}

function updateProgressBar(progress) {
	let bar = document.getElementById("progressBar");
	if (progress == 1) {
		bar.style.opacity = 0;
	} else {
		bar.style.opacity = 1;
		bar.style.width = (progress * 100) + "%";
	}
}

function initialProgress(error, progress, key) {
	if (error) {
		console.log("Error");
	}
	else if (key) {
		bigHash = getB64(key);
		switchToSecondaryView(key);

		// Android app
		if (!(typeof Android === "undefined" || Android === null))
			Android.saveKey(bigHash);
	}
	else {
		updateProgressBar(progress);
	}
}

function switchToSecondaryView(key) {
	switchSection();
	document.getElementById("reloadButton").style.display = "unset";
	if (settings.master.showHashcheck) {
		scrypt(key,
			getBuffer("hashcheck"), 2048, 8, 8, 3,
			function (e, p, k) {
				if (k) {
					document.getElementById("hashcheckBar").style.background = "rgb(" + k + ")";
				}
			});
	}
}

function switchSection() {
	refreshSavedAccounts();
	uninitializedSection.style.opacity = "0";
	setTimeout(function () {
		uninitializedSection.style.display = "none";
		initializedSection.style.display = "unset";
		setTimeout(function () {
			initializedSection.style.opacity = "1";
			document.getElementById("account").focus();
		}, 16);
	}, 200);
}

function initialize() {
	let ms = settings.master;
	let masterPassword = document.getElementById("masterPassword");
	let confirmMasterPassword = document.getElementById("confirmMasterPassword");

	if (validatePassword(masterPassword, confirmMasterPassword, ms)) {
		let masterBuffer = getBuffer(masterPassword.value);
		let saltBuffer = getBuffer(ms.salt);
		masterPassword.value = "";

		scrypt(Array.from(masterBuffer), saltBuffer, ms.N, ms.r, ms.p, ms.dkLen, initialProgress);

		console.log("Initializing");

		document.getElementById("initializeButton").disabled = true;
	}
}

function generateProgress(error, progress, key) {
	if (error) {
		console.log("Error");
		document.getElementById("generateButton").disabled = false;
	}
	else if (key) {
		document.getElementById("accountPassword").value = getB64(key);
		document.getElementById("copyButton").focus();
		document.getElementById("generateButton").disabled = false;
	}
	else {
		updateProgressBar(progress);
	}
}

function generate() {
	let ss = settings.secondary;
	let account = document.getElementById("account");
	let secondaryPassword = document.getElementById("secondaryPassword");
	let confirmSecondaryPassword = document.getElementById("confirmSecondaryPassword");

	if (validatePassword(secondaryPassword, confirmSecondaryPassword, ss)) {
		let combinedBuffer = getBuffer(bigHash + account.value + secondaryPassword.value);
		let saltBuffer = getBuffer(ss.salt);
		if (ss.clearSecondaryPassword)
			secondaryPassword.value = "";

		scrypt(Array.from(combinedBuffer), saltBuffer, ss.N, ss.r, ss.p, ss.dkLen, generateProgress);

		document.getElementById("generateButton").disabled = true;

		console.log("Generating");
	}
}

function validatePassword(password, confirmPassword, settings) {
	let errorLabel = document.getElementById("passwordsDontMatchLabel");

	if (confirmPassword.value == ""
		|| password.value == confirmPassword.value) {
		errorLabel.style.opacity = "0";
		// errorLabel.style.height = "0";

		if (settings.clearSecondaryPassword
			|| settings.clearSecondaryPassword === undefined)
			confirmPassword.value = "";

		return true;
	}
	else {
		errorLabel.style.opacity = "1";
		// errorLabel.style.height = "10pt";
		password.focus();
		password.select();
		return false;
	}
}

function clearPassword() {
	document.getElementById("accountPassword").value = placeholderText;
}

function copyPassword() {
	let pf = document.getElementById("accountPassword");
	pf.type = "text";
	pf.select();

	try {
		let result = document.execCommand("copy");
		console.log(result ? "Copied to clipboard" : "Failed copying to clipboard");
		if (result) {
			clearPassword();
		}
	}
	catch (err) {
		console.log("Error");
	}

	pf.type = "password";

	let acc = document.getElementById("account");
	acc.focus();
	acc.select();
}

function showPassword(element) {
	let input = element.previousElementSibling;
	if (input.type == "password") {
		input.type = "text";
		element.firstElementChild.src = "icons/eye-with-a-diagonal-line-interface-symbol-for-invisibility.svg";
	}
	else {
		input.type = "password";
		element.firstElementChild.src = "icons/eye-open.svg";
	}
}

function acceptCookies() {
	settings.acceptedCookies = true;
	saveCookie();
	hideCookieBanner();
}

function hideCookieBanner() {
	document.getElementById("cookieBanner")?.remove()
}

function reload() {
	setTimeout(function () { location.reload(); }, 200);
	document.getElementById("main").style.opacity = "0";
	// Android app
	if (!(typeof Android === "undefined" || Android === null))
		Android.saveKey(null);
}

function refreshSavedAccounts() {
	let datalist = document.getElementById("savedAccounts");

	while (datalist.hasChildNodes()) {
		datalist.removeChild(datalist.lastChild);
	}

	for (let entry of settings.savedAccounts) {
		let option = document.createElement("option");
		option["value"] = entry;
		datalist.appendChild(option);
	}
}

function saveAccount() {
	let account = document.getElementById("account").value;
	if (account && account.length > 0 && !settings.savedAccounts.includes(account)) {
		settings.savedAccounts.push(account);
		if (settings.acceptedCookies) {
			saveCookie();
		}
		refreshSavedAccounts();
	}
}

function showAccountManager() {
	let settingsMaster = document.getElementById("settings-master");
	let settingsSecondary = document.getElementById("settings-secondary");
	settingsMaster.style.height = "0vh";
	settingsSecondary.style.height = "0vh";

	let accounts = document.getElementById("accountsToManage");
	accounts.style.maxHeight = "70vh";

	let button = document.getElementById("manageAccountsButton");
	button.innerText = "← Back"

	if (settings.savedAccounts.length > 0) {
		while (accounts.hasChildNodes()) {
			accounts.removeChild(accounts.lastChild);
		}

		for (let entry of settings.savedAccounts) {
			let para = document.createElement("p");

			let button = document.createElement("button");
			button.classList.add("deleteAccountButton");
			button.onclick = deleteAccount;
			let buttonText = document.createTextNode("×");

			let span = document.createElement("span");
			let text = document.createTextNode(entry);

			span.appendChild(text);
			button.appendChild(buttonText);
			para.appendChild(button);
			para.appendChild(span);
			accounts.appendChild(para);
		}
	}
	else {
		accounts.appendChild(document.createTextNode("No saved accounts"));
	}
	accountsManaged = true;
}

function hideAccountManager() {
	let settingsMaster = document.getElementById("settings-master");
	let settingsSecondary = document.getElementById("settings-secondary");
	settingsMaster.style.height = masterSettingsHeight + "px";
	settingsSecondary.style.height = secondarySettingsHeight + "px";

	let accounts = document.getElementById("accountsToManage");
	accounts.style.maxHeight = "0vh";

	let button = document.getElementById("manageAccountsButton");
	button.innerText = "Manage accounts"

	accountsManaged = false;
}

function manageAccounts() {
	if (!accountsManaged) {
		showAccountManager();
	}
	else {
		hideAccountManager();
	}
}

function deleteAccount(e) {
	let node = e.target.parentElement;
	let account = node.lastChild.innerText;

	let index = settings.savedAccounts.indexOf(account);
	if (index >= 0) {
		settings.savedAccounts.splice(index, 1);
	}

	node.parentElement.removeChild(node);
}

// Android app
function skipMasterPassword(key) {
	bigHash = getB64(key);
	switchToSecondaryView(key);
}

window.onload = start;
window.addEventListener("pageshow", function (event) {
	var historyTraversal = event.persisted ||
		(typeof window.performance != "undefined" &&
			window.performance.navigation.type === 2);

	if (historyTraversal) {
		console.log("Got here via the back button. Reloading...");
		window.location.reload(true);
	}
});
