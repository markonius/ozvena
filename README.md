# Ozvena

Ozvena is a secure hashing password manager using the scrypt algorithm. It is inspired by a paper by J. Alex Halderman, Brent Waters and Edward W. Felten: "A Convenient Method for Securely Managing Passwords".

If you are interested in understanding the algorithm, here is a presentation from the author:  
[https://www.tarsnap.com/scrypt/scrypt-slides.pdf](https://www.tarsnap.com/scrypt/scrypt-slides.pdf)

If you are interested in why this app works the way it does, here is the above mentioned paper:  
[http://www.cs.utexas.edu/~bwaters/publications/papers/www2005.pdf](http://www.cs.utexas.edu/~bwaters/publications/papers/www2005.pdf)

If you want to know how to use the app, keep reading.

## How to use Ozvena

Choose a strong master password. Clicking "Initialize" will start the hashing of the password.  
This will take anywhere between a few seconds to a minute, depending on your device.  
This will effectively mark your device as "trusted" (well, at least until you close the browser tab).

Leave the browser tab open and rely on the secondary password to keep your accounts safe.  
Enter the name of the account for which you wish to generate a password, or anything else that uniquely identifies this password.  
The secondary password can be the same as the primary, since it is hashed again, with different settings.  
Click generate to get a password for your account.

__[Ozvena can be found here.](https://markonius.gitlab.io/ozvena/ozvena.html)__

## Special thanks

[Marko Žitković](https://gitlab.com/puzomorcro) - Found a [bug with Chromium](https://bugs.chromium.org/p/chromium/issues/detail?id=918351)